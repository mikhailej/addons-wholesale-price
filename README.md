# Module: wholesale price #

Module wholesale price for CS-Cart. This module allows you to derive the wholesale price of goods for different groups of buyers.

### Installing the module ###

To install this module, copy all the files to / cs-cart / with the folder structure preserved.
After that, go to managing the modules, install it and turn it on.
In the module settings, you can set which groups of users to display the wholesale price.
